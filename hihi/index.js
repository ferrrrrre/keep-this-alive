//REQUIRED extensions //
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mongo = require('mongodb').MongoClient;

// <!-- end of REQUIRED --> //

var numberOfUsers = 0;
var nicknames = [];
var db_url = "mongodb://KTA:KTA@ds037622.mongolab.com:37622/db_kta"; 

app.get('/', function (req, res){
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket){
  numberOfUsers += 1;
  console.log(numberOfUsers);
  console.log(socket.id);
  io.emit('chat message', numberOfUsers + ' users connected');

  socket.on('disconnect', function(){
    console.log('user disconnected');
      numberOfUsers -= 1;
      io.emit('chat message', numberOfUsers + ' users connected');
  });

  socket.on('chat message', function (msg){
    console.log('message: ' + msg);
    io.emit('chat message', socket.id+": "+msg);

  //Dingen opslaan naar de db
  mongo.connect(db_url, function (err, db){
    var collection = db.collection('chat_messages');
    collection.insert({content: msg}, function(err, o){
      if(err){console.warn(err.message); }
      else{ console.log("chat message inserted into db: " + msg); }
    });
  });

  //dingen uit de db halen
  mongo.connect(db_url, function (err, db) {
    var collection = db.collection('chat_messages')
    var stream = collection.find().sort({ _id : -1 }).limit(10).stream();
    stream.on('data', function (chat) { socket.emit('chat message', chat.content); });
    //io.emit naar allemaal, socket.emit enkel naar jou 
  });

  });
    
});


// HTTP CONNECTION STUFF //
http.listen(3000, function(){
  console.log('listening on *:3000');
});

